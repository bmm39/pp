# Pet project

# Python:
version > 3.7

## Start db:
docker-compose up -d

## Migrations:
alembic upgrade head

## Install pre-commit:
pre-commit install

## Run app
python ./pp/main.py
