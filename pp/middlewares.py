import jwt
from aiohttp.web_response import json_response
from aiohttp import web

from models.users import users
from settigns import config


@web.middleware
async def auth_middleware(request, handler):
    request.user = None
    jwt_token = request.headers.get("authorization", None)
    if jwt_token:
        try:
            jwt_config = config["JWT"]
            payload = jwt.decode(
                jwt_token,
                jwt_config["secret"],
                algorithms=[jwt_config["algorithm"]],
            )
        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            return json_response({"message": "Token is invalid"}, status=400)

        async with request.app["db"].acquire() as conn:

            user_id = payload["user_id"]
            cursor = await conn.execute(users.select().where(users.c.id == user_id))
            records = await cursor.fetchall()
            request.user = records[0]
    return await handler(request)