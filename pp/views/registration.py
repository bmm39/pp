from aiohttp.web_request import Request
from aiohttp.web_response import json_response
from aiohttp_validate import validate
from pymysql import IntegrityError

from models.users import RegistrationData, insert_user
from shemes.registration import REGISTRATION_JSON_SCHEMA


@validate(request_schema=REGISTRATION_JSON_SCHEMA)
async def registration(request_data, request: Request):
    async with request.app["db"].acquire() as conn:
        registration_data = RegistrationData.from_dict(request_data)
        try:
            await insert_user(connection=conn, registration_data=registration_data)
            return json_response(data={"data": "User successfully created"})
        except IntegrityError as exception:
            return json_response(data={"exception": str(exception)}, status=422)
