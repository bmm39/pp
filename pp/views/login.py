import asyncio

import jwt
import bcrypt

from datetime import datetime, timedelta
from aiohttp.web_request import Request
from aiohttp.web_response import json_response, Response
from aiohttp_validate import validate
from models.users import LoginData, get_user_by_email, users
from settigns import config
from shemes.login import LOGIN_JSON_SCHEMA


@validate(request_schema=LOGIN_JSON_SCHEMA)
async def login(request_data, request: Request):
    async with request.app["db"].acquire() as conn:
        jwt_config = config["JWT"]
        login_data = LoginData.from_dict(request_data)
        records = await get_user_by_email(connection=conn, email=login_data.email)
        loop = asyncio.get_running_loop()
        check_password = await loop.run_in_executor(
            None,
            bcrypt.checkpw,
            login_data.password.encode(),
            records[0][users.c.password].encode(),
        )
        if len(records) == 1 and check_password:
            payload = {
                "user_id": records[0][users.c.id],
                "exp": datetime.utcnow()
                + timedelta(seconds=jwt_config["exp_seconds_delta"]),
            }
            jwt_token = jwt.encode(
                payload, jwt_config["secret"], jwt_config["algorithm"]
            )
            return json_response({"token": jwt_token.decode("utf-8")})
        return Response(body="User not found", status=404)
