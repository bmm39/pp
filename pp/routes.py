from views.login import login
from views.registration import registration


def setup_routes(app):
    app.router.add_post('/login', login)
    app.router.add_post('/registration', registration)
