from sqlalchemy import MetaData
from aiomysql.sa import create_engine

meta = MetaData()


async def init_db(app):
    conf = app["config"]["mysql"]
    engine = await create_engine(
        user=conf["user"],
        db=conf["database"],
        host=conf["host"],
        port=conf["port"],
        password=conf["password"],
        autocommit=True,
    )
    app["db"] = engine


async def close_db(app):
    app["db"].close()
    await app["db"].wait_closed()
