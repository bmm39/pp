import asyncio
import bcrypt

from dataclasses import dataclass
from typing import Any
from sqlalchemy import Table, Column, Integer, String, Float, text
from sqlalchemy.dialects.mysql import TIMESTAMP
from db import meta

users = Table(
    "users",
    meta,
    Column("id", Integer, primary_key=True),
    Column("login", String(32), nullable=False, unique=True),
    Column("password", String(512), nullable=False),
    Column("birthday", String(128), nullable=False),
    Column("weight", Float, nullable=False),
    Column("height", Float, nullable=False),
    Column("activity_rate", Integer, nullable=False),
    Column(
        "created_at",
        TIMESTAMP,
        nullable=False,
        server_default=text("CURRENT_TIMESTAMP"),
    ),
    Column(
        "updated_at",
        TIMESTAMP,
        nullable=False,
        server_default=text("CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
    ),
)


@dataclass
class RegistrationData:
    email: str
    password: str
    birthday: str
    weight: int
    height: int
    activity_rate: int

    @staticmethod
    def from_dict(obj: Any) -> "RegistrationData":
        assert isinstance(obj, dict)
        email = obj.get("email")
        password = obj.get("password")
        birthday = obj.get("birthday")
        weight = obj.get("weight")
        height = obj.get("height")
        activity_rate = obj.get("activity_rate")
        return RegistrationData(
            email, password, birthday, weight, height, activity_rate
        )


@dataclass
class LoginData:
    email: str
    password: str

    @staticmethod
    def from_dict(obj: Any) -> "LoginData":
        assert isinstance(obj, dict)
        email = obj.get("email")
        password = obj.get("password")
        return LoginData(email, password)


async def insert_user(connection, registration_data: RegistrationData):
    loop = asyncio.get_running_loop()
    salt = await loop.run_in_executor(None, bcrypt.gensalt)
    hashed_password = await loop.run_in_executor(
        None, bcrypt.hashpw, registration_data.password.encode(), salt
    )
    query = users.insert().values(
        login=registration_data.email,
        password=hashed_password,
        birthday=registration_data.birthday,
        weight=registration_data.weight,
        height=registration_data.height,
        activity_rate=registration_data.activity_rate,
    )
    return await connection.execute(query)


async def get_user_by_email(connection, email):
    query = users.select().where(users.c.login == email)
    cursor = await connection.execute(query)
    records = await cursor.fetchall()
    return records
