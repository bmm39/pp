from sqlalchemy import (
    Table,
    Column,
    ForeignKey,
    Integer,
    Float,
    TIMESTAMP,
)

from db import meta

food_nutrition = Table(
    "food_nutrition",
    meta,
    Column("created_at", TIMESTAMP, nullable=False),
    Column("updated_at", TIMESTAMP, nullable=False),
    Column("id", Integer, primary_key=True),
    Column("protein", Float, nullable=False),
    Column("fats", Float, nullable=False),
    Column("carbohydrates", Float, nullable=False),
    Column("food_id", Integer, ForeignKey("food.id", ondelete="CASCADE")),
)