from sqlalchemy import (
    Table,
    Column,
    ForeignKey,
    Integer,
    TIMESTAMP,
)

from db import meta

user_meals = Table(
    "user_meals",
    meta,
    Column("id", Integer, primary_key=True),
    Column("created_at", TIMESTAMP, nullable=False),
    Column("updated_at", TIMESTAMP, nullable=False),
    Column("user_id", Integer, ForeignKey("users.id", ondelete="CASCADE")),
    Column("food_id", Integer, ForeignKey("food.id", ondelete="CASCADE")),
)