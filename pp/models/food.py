from sqlalchemy import (
    Table,
    Column,
    Integer,
    String,
    TIMESTAMP,
)

from db import meta

food = Table(
    "food",
    meta,
    Column("id", Integer, primary_key=True),
    Column("created_at", TIMESTAMP, nullable=False),
    Column("updated_at", TIMESTAMP, nullable=False),
    Column("name", String(100), nullable=False),
    Column("description", String(500), nullable=False, default=""),
)