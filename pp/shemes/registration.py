REGISTRATION_JSON_SCHEMA = {
    "type": "object",
    "properties": {
        "email": {"type": "string"},
        "password": {"type": "string"},
        "birthday": {"type": "string"},
        "weight": {"type": "number"},
        "height": {"type": "number"},
        "activity_rate": {"type": "number"},
    },
    "required": ["email", "password", "birthday", "weight", "height", "activity_rate"],
    "additionalProperties": False,
}
