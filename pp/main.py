import aiohttp_debugtoolbar
import asyncio
import uvloop

from aiohttp import web
from middlewares import auth_middleware
from settigns import config
from routes import setup_routes
from db import init_db, close_db

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

app = web.Application(middlewares=[auth_middleware])
setup_routes(app)
aiohttp_debugtoolbar.setup(app)
app["config"] = config
app.on_startup.append(init_db)
app.on_cleanup.append(close_db)
web.run_app(app, port=9090)
